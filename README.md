
## How to decrypt

Step 1: Fetch the data
```
wget --output-document=logo-source.jpg.enc https://gateway.ipfs.io/ipfs/QmdeHTURq15xXddgxBsCz9c2ChyH3itD1o9fjyCYGe8utg 
```
Step 2: Decrypt data with password
```
pip install -r requirements.txt
python decrypt.py logo-source.jpg.enc <password>
```
